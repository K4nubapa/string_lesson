﻿#include <string>
#include <iostream>

int main()
{
    std::cout << "Type smth: ";
    std::string stroke;
    std::getline(std::cin, stroke);
    char first = stroke.front();
    char last = stroke.back();

    std::cout << "Count of symbols in string: " << stroke.length() << "\n";
    std::cout << "First symbol in string: " << first << "\n";
    std::cout << "Last symbol in string: " << last;

}
